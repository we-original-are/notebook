class Note {
  late int? noteId;
  late String noteText;
  late String? noteCreatedTime;
  late String? noteEditedTime;
  late int? seeMore = 0;

  Note(
      {this.noteId,
      required this.noteText,
      required this.seeMore,
      this.noteCreatedTime,
      this.noteEditedTime});

  ///در هنگام درج یک ردیف در جدول مورد استفاده قرار گیرد

  Map<String, dynamic> toMapWithoutId() {
    final map = new Map<String, dynamic>();
    map["noteText"] = noteText;
    map["seeMore"] = seeMore;
    map["noteCreatedTime"] = noteCreatedTime;
    map["noteEditedTime"] = noteEditedTime;
    return map;
  }

  ///به هنگام به روز رسانی یک ردیف در جدول مورد استفاده قرار گیرد

  Map<String, dynamic> toMap() {
    final map = new Map<String, dynamic>();
    map["noteId"] = noteId;
    map["noteText"] = noteText;
    map["seeMore"] = seeMore;
    map["noteCreatedTime"] = noteCreatedTime;
    map["noteEditedTime"] = noteEditedTime;
    return map;
  }

  ///در هنگام تبدیل ردیف به شیء مورد استفاده قرار گیرد

  factory Note.fromMap(Map<String, dynamic> data) => new Note(
      noteId: data['noteId'],
      noteText: data['noteText'],
      seeMore: data['seeMore'],
      noteCreatedTime: data['noteCreatedTime'],
      noteEditedTime: data['noteEditedTime']);
}
