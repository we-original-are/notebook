import 'dart:async';

import 'package:flutter/material.dart';
import 'Note.dart';
import 'localization/language/languages.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key, required this.listNotes}) : super(key: key);
  final List<Note> listNotes;

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _showBody = false;

  void delayTime() async {
    Timer(Duration(milliseconds: 2000), () {
      if (widget.listNotes != null) {
        setState(() {
          _showBody = true;
        });
      }
    });
  }

  @override
  void initState() {
    delayTime();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width,
        height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              color: Colors.black,
              image: DecorationImage(
                  image: AssetImage('images/pic1.jpg'), fit: BoxFit.cover)),
          child: _showBody
              ? Container(
                  width: width,
                  height: height,
                  color: Colors.black.withOpacity(.4),
                  padding:
                      EdgeInsets.only(top: 10, bottom: 20, right: 10, left: 10),
                  child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.sticky_note_2_outlined,
                          size: 100,
                          color: Colors.white,
                        ),
                        Text(Languages.of(context)!.title,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 35,
                                fontWeight: FontWeight.bold)),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: Text(
                              widget.listNotes.length > 0
                                  ? Languages.of(context)!.fullNoteSubTitle
                                  : Languages.of(context)!.emptyNoteSubTitle,
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black54,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ],
                    ),
                  ),
                )
              : Text(''),
        ),
      ),
    );
  }
}
