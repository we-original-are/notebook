import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:notebook/Note.dart';
import 'DatabaseHelper.dart';
import 'localization/local_constant.dart';
import 'localization/localization_delegate.dart';
import 'notes_list_screen.dart';
import 'splash_screen.dart';

void main() {
  runApp(MyApp(
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();

  static void setLocale(BuildContext context, Locale newLocale) {
    var state = context.findAncestorStateOfType<_MyAppState>();
    state!.setLocale(newLocale);
  }
}

class _MyAppState extends State<MyApp> {
  Locale _locale = Locale('fa', '');
  bool homeState = false;
  int _index = 0;
  List<Note> listNotes = [];
  Random random = new Random();

  List<MaterialColor> _colors = <MaterialColor>[
    Colors.blue,
    Colors.deepPurple,
    Colors.red,
    Colors.green,
    Colors.pink,
    Colors.amber,
    Colors.lightBlue,
    Colors.purple,
  ];

  void _changeIndex() async {
    setState(() {
      _index = random.nextInt(_colors.length - 1);
    });
  }

  @override
  void initState() {
    _getLocale();
    replacePage();
    _changeIndex();
    getNotes();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        builder: (context, child) {
          return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: child!);
        },
        localizationsDelegates: [
          AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          Locale('fa', ''),
          Locale('en', ''),
        ],
        debugShowCheckedModeBanner: false,
        locale: _locale,
        localeResolutionCallback: (locale, supportedLocales) {
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale!.languageCode &&
                supportedLocale.countryCode == locale.countryCode) {
              return supportedLocale;
            }
          }
          return supportedLocales.first;
        },
        theme: ThemeData(
          fontFamily: "fontFa1",
          primarySwatch: _colors[_index],
        ),
        home: homeState
            ? NotesList()
            : SplashScreen(
          listNotes: listNotes,
        ));
  }

  void replacePage() async {
    Timer(Duration(seconds: 9), () {
      setState(() {
        homeState = true;
      });
    });
  }

  Future<void> getNotes() async {
    List<Map<String, dynamic>> listMap =
    await DatabaseHelper.instance.queryAllRows();
    setState(() {
      listMap.forEach((map) => listNotes.add(Note.fromMap(map)));
    });
  }

  void setLocale(Locale locale) async {
    setState(() {
      _locale = locale;
    });
  }

  void _getLocale() async {
    await getLocale().then((localeResponse) {
      setState(() {
        _locale = localeResponse;
      });
    });
  }
}
