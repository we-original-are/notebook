import 'package:flutter/material.dart';
import 'package:notebook/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String prefsSelectLanguageCode = "SelectedLanguageCode";

Future<Locale> setLocale(String languageCode) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.setString(prefsSelectLanguageCode, languageCode);
  return _locale(languageCode);
}

Future<Locale> getLocale() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String languageCode = _prefs.getString(prefsSelectLanguageCode) ?? "fa";
  return _locale(languageCode);
}

Locale _locale(String languageCode) {
  return languageCode != null && languageCode.isNotEmpty
      ? Locale(languageCode, '')
      : Locale('fa', '');
}

void changeLanguage(BuildContext context, String selectLanguageCode) async {
  var _locale = await setLocale(selectLanguageCode);
  MyApp.setLocale(context, _locale);
}
