import 'package:flutter/material.dart';

abstract class Languages {
  static Languages? of(BuildContext context) {
    return Localizations.of<Languages>(context, Languages);
  }

  String get title;

  String get fullNoteSubTitle;

  String get emptyNoteSubTitle;

  String get noNotesToDisplay;

  String get eyeTooltip;

  String get floatingTooltip;

  String get textFieldHintText;

  String get labelSelectLanguage;

  String get yes;

  String get cancel;

  String get save;

  String get options;

  String get show;

  String get edit;

  String get delete;

  String get deleteMessage;

  String get seeMore;

  String get createdIn;

  String get editedIn;
}
