import 'package:notebook/localization/language/languages.dart';

class LanguageEn extends Languages {
  @override
  // TODO: implement cancel
  String get cancel => "Cancel";

  @override
  // TODO: implement delete
  String get delete => "Delete";

  @override
  // TODO: implement deleteMessage
  String get deleteMessage => "Do you want delete this note ?";

  @override
  // TODO: implement edit
  String get edit => "Edit";

  @override
  // TODO: implement emptyNoteSubTitle
  String get emptyNoteSubTitle => "Save your note here!";

  @override
  // TODO: implement eyeTooltip
  String get eyeTooltip => "Change Theme Color";

  @override
  // TODO: implement floatingTooltip
  String get floatingTooltip => "Add Note";

  @override
  // TODO: implement fullNoteSubTitle
  String get fullNoteSubTitle => "Your note is here!";

  @override
  // TODO: implement noNotesToDisplay
  String get noNotesToDisplay =>
      " You have no notes!\n Now let's create a new note";

  @override
  // TODO: implement options
  String get options => "Options";

  @override
  // TODO: implement save
  String get save => "Save";

  @override
  // TODO: implement show
  String get show => "Show";

  @override
  // TODO: implement textFieldHintText
  String get textFieldHintText => " Enter your note ...";

  @override
  // TODO: implement title
  String get title => "Notes";

  @override
  // TODO: implement yes
  String get yes => "Yes";

  @override
  // TODO: implement seeMore
  String get seeMore => "... see more";

  @override
  // TODO: implement createdIn
  String get createdIn => "Created in :  ";

  @override
  // TODO: implement editedIn
  String get editedIn => "Edited in : ";

  @override
  // TODO: implement labelSelectLanguage
  String get labelSelectLanguage => "Select Language";
}
