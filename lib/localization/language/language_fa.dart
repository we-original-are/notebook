import 'package:notebook/localization/language/languages.dart';

class LanguageFa extends Languages {
  @override
  // TODO: implement cancel
  String get cancel => "لغو";

  @override
  // TODO: implement delete
  String get delete => "حذف";

  @override
  // TODO: implement deleteMessage
  String get deleteMessage => "میخواهید این یادداشت را حذف کنید ؟";

  @override
  // TODO: implement edit
  String get edit => "ویرایش";

  @override
  // TODO: implement emptyNoteSubTitle
  String get emptyNoteSubTitle => "یادداشت خود را اینجا ذخیره کنید!";

  @override
  // TODO: implement eyeTooltip
  String get eyeTooltip => "تغییر رنگ تم";

  @override
  // TODO: implement floatingTooltip
  String get floatingTooltip => "افزودن یادداشت";

  @override
  // TODO: implement fullNoteSubTitle
  String get fullNoteSubTitle => "یادداشت شما اینجاست!";

  @override
  // TODO: implement noNotesToDisplay
  String get noNotesToDisplay =>
      " شما هیچ یادداشتی ندارید!\n حالا بیایید یک یادداشت جدید ایجاد کنیم";

  @override
  // TODO: implement options
  String get options => "گزینه ها";

  @override
  // TODO: implement save
  String get save => "ذخیره";

  @override
  // TODO: implement show
  String get show => "نمایش";

  @override
  // TODO: implement textFieldHintText
  String get textFieldHintText => " یادداشت خود را وارد کنید ...";

  @override
  // TODO: implement title
  String get title => "یادداشت";

  @override
  // TODO: implement yes
  String get yes => "بله";

  @override
  // TODO: implement seeMore
  String get seeMore => "مشاهده بیشتر ...";

  @override
  // TODO: implement createdIn
  String get createdIn => "ایجاد شده در : ";

  @override
  // TODO: implement editedIn
  String get editedIn => "ویرایش شده در : ";

  @override
  // TODO: implement labelSelectLanguage
  String get labelSelectLanguage => "انتخاب زبان";
}
