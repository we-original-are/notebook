import 'package:flutter/material.dart';
import 'package:notebook/DatabaseHelper.dart';
import 'Note.dart';
import 'localization/language/languages.dart';
import 'main.dart';
import 'notes_list_screen.dart';
import 'package:intl/intl.dart';

class AddEditNote extends StatefulWidget {
  final Note? selectedNote;
  final bool isEdit;

  const AddEditNote({Key? key, this.selectedNote, required this.isEdit})
      : super(key: key);

  @override
  _AddEditNoteState createState() => _AddEditNoteState();
}

class _AddEditNoteState extends State<AddEditNote> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController controllerName = TextEditingController();
  bool onlyShowText = true;

  @override
  Widget build(BuildContext context) {
    if (widget.isEdit) {
      controllerName.text = widget.selectedNote!.noteText;
    }
    double heightX = MediaQuery.of(context).size.height,
        widthX = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        actions: [
          onlyShowText
              ? Text(
                  "",
                  style: TextStyle(fontSize: 0),
                )
              : ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    Languages.of(context)!.cancel,
                  )),
          onlyShowText
              ? Text(
                  "",
                  style: TextStyle(fontSize: 0),
                )
              : ElevatedButton(
                  onPressed: () {
                    if (controllerName.text.isNotEmpty) {
                      String now = DateFormat("yyyy/MM/dd  -  HH:mm:ss")
                          .format(DateTime.now())
                          .toString();
                      if (widget.isEdit) {
                        if (widget.selectedNote!.noteText.characters ==
                            controllerName.text.characters) {
                          Navigator.pop(context);
                        } else {
                          Note updateNote = Note(
                            noteEditedTime: now,
                            noteCreatedTime:
                                widget.selectedNote!.noteCreatedTime,
                            noteId: widget.selectedNote!.noteId,
                            noteText: controllerName.text,
                            seeMore: 0,
                          );
                          DatabaseHelper.instance.update(updateNote.toMap());
                        }
                      } else {
                        Note addNote = Note(
                            noteText: controllerName.text,
                            seeMore: 0,
                            noteCreatedTime: now,
                            noteEditedTime: "0");

                        DatabaseHelper.instance
                            .insert(addNote.toMapWithoutId());
                      }
                      Route newRoute = MaterialPageRoute(builder: (context) {
                        return NotesList();
                      });
                      Navigator.pushAndRemoveUntil(
                          context, newRoute, (route) => false);
                    }
                  },
                  child: Text(Languages.of(context)!.save))
        ],
      ),
      body: Container(
        height: heightX,
        width: widthX,
        child: Form(
          key: _formKey,
          child: TextFormField(
            onTap: () {
              if (onlyShowText) {
                setState(() {
                  onlyShowText = false;
                });
              }
            },
            readOnly: onlyShowText,
            controller: controllerName,
            style: TextStyle(fontSize: 18, height: 1.5, color: Colors.black87),
            minLines: heightX.toInt(),
            maxLines: null,
            decoration: InputDecoration(
              hintText: Languages.of(context)!.textFieldHintText,
              contentPadding: EdgeInsets.all(15),
              border: InputBorder.none,
            ),
          ),
        ),
      ),
    );
  }
}
