import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notebook/DatabaseHelper.dart';
import 'package:notebook/add_edit_note.dart';
import 'package:notebook/localization/language/languages.dart';

import 'Note.dart';
import 'language_Data.dart';
import 'localization/local_constant.dart';

class NotesList extends StatefulWidget {
  const NotesList({Key? key}) : super(key: key);

  @override
  _NotesListState createState() => _NotesListState();
}

class _NotesListState extends State<NotesList> {
  List<Note> listNotes = [];
  bool _showBody = false;

  Future<void> getNotes() async {
    List<Map<String, dynamic>> listMap =
        await DatabaseHelper.instance.queryAllRows();
    setState(() {
      listMap.forEach((map) => listNotes.add(Note.fromMap(map)));
    });
  }

  testData() async {
    Timer(Duration(milliseconds: 50), () {
      setState(() {
        _showBody = true;
      });
    });
  }

  @override
  void initState() {
    testData();
    getNotes();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.sticky_note_2_outlined,
              color: Theme.of(context).primaryIconTheme.color,
            ),
            SizedBox(
              width: 10,
            ),
            Text(Languages.of(context)!.title),
          ],
        ),
        actions: [
          PopupMenuButton<LanguageData>(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            icon: Icon(Icons.more_vert),
            tooltip: Languages.of(context)!.labelSelectLanguage,
            onSelected: (LanguageData? language) {
              changeLanguage(context, language!.languageCode);
            },
            itemBuilder: (context) => LanguageData.languageList()
                .map<PopupMenuItem<LanguageData>>((e) =>
                    PopupMenuItem<LanguageData>(value: e, child: Text(e.flag)))
                .toList(),
          ),
        ],
      ),
      body: _showBody
          ? (listNotes.length > 0
              ? SafeArea(
                  child: Scrollbar(
                    thickness: 4,
                    child: ListView.separated(
                      padding: EdgeInsets.only(
                        top: 10,
                        bottom: 70,
                      ),
                      itemCount: listNotes.length,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (context, index) {
                        Note getNote = listNotes[index];
                        String _timeCreated =
                            listNotes[index].noteCreatedTime.toString();

                        String _timeEdited =
                            listNotes[index].noteEditedTime.toString();

                        return InkWell(
                          child: Container(
                            color: Colors.blueGrey,
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 10, right: 10, left: 10),
                                  child: Text(
                                    getNote.noteText.length >= 200
                                        ? getNote.seeMore == 0
                                            ? getNote.noteText
                                                .substring(0, 150)
                                                .replaceRange(
                                                    150,
                                                    150,
                                                    Languages.of(context)!
                                                        .seeMore)
                                            : getNote.noteText
                                        : getNote.noteText,
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  margin: EdgeInsets.zero,
                                  color: Theme.of(context).primaryColor,
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        Languages.of(context)!.createdIn +
                                            _timeCreated,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Theme.of(context)
                                                .primaryIconTheme
                                                .color),
                                      ),
                                      _timeEdited.length > 1
                                          ? Divider(
                                              color: Theme.of(context)
                                                  .primaryIconTheme
                                                  .color,
                                              thickness: 1.2,
                                            )
                                          : Text(
                                              "",
                                              style: TextStyle(fontSize: 0),
                                            ),
                                      _timeEdited.length > 1
                                          ? Text(
                                              Languages.of(context)!.editedIn +
                                                  _timeEdited,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Theme.of(context)
                                                      .primaryIconTheme
                                                      .color),
                                            )
                                          : Text(
                                              "",
                                              style: TextStyle(fontSize: 0),
                                            )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              listNotes[index].seeMore =
                                  listNotes[index].seeMore == 0 ? 1 : 0;
                            });
                          },
                          onDoubleTap: () {
                            optionDialog(context, getNote);
                          },
                          onLongPress: () {
                            optionDialog(context, getNote);
                          },
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return const Divider(
                          height: 20,
                          color: Colors.black,
                        );
                      },
                    ),
                  ),
                )
              : Center(
                  child: GestureDetector(
                    onTap: () {
                      addNewNote();
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.sticky_note_2_outlined,
                          size: 100,
                          color: Theme.of(context).primaryColor,
                        ),
                        Card(
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              Languages.of(context)!.noNotesToDisplay,
                              style: TextStyle(
                                  color:
                                      Theme.of(context).primaryIconTheme.color,
                                  fontSize: 18),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ))
          : Text(""),
      floatingActionButton: FloatingActionButton(
        tooltip: Languages.of(context)!.floatingTooltip,
        onPressed: () {
          addNewNote();
        },
        child: Icon(Icons.edit),
      ),
    );
  }

  optionDialog(context, Note getNote) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            title: Container(
                padding: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        topLeft: Radius.circular(10))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      Languages.of(context)!.options,
                      style: TextStyle(
                          color: Theme.of(context).primaryIconTheme.color),
                    ),
                    CloseButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: Theme.of(context).primaryIconTheme.color,
                    )
                  ],
                )),
            titlePadding: EdgeInsets.zero,
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)))),
                        onPressed: () {
                          Navigator.pop(context);
                          Route route = MaterialPageRoute(builder: (context) {
                            return AddEditNote(
                                selectedNote: getNote, isEdit: true);
                          });
                          Navigator.push(context, route);
                        },
                        child: Text(
                            "${Languages.of(context)!.show} - ${Languages.of(context)!.edit}"))),
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)))),
                        onPressed: () {
                          Navigator.pop(context);
                          deleteDialog(getNote);
                        },
                        child: Text(
                          Languages.of(context)!.delete,
                        ))),
              ],
            ),
          );
        });
  }

  deleteDialog(Note getNote) {
    double width = MediaQuery.of(context).size.width;
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            contentPadding: EdgeInsets.zero,
            titlePadding: EdgeInsets.zero,
            title: Container(
              width: width,
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0))),
              child: Text(Languages.of(context)!.deleteMessage,
                  style: TextStyle(
                      fontSize: 20,
                      color: Theme.of(context).primaryIconTheme.color)),
            ),
            content: SingleChildScrollView(
              child: Card(
                  color: Colors.blueGrey,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      getNote.noteText,
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  )),
            ),
            actions: [
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)))),
                  onPressed: () {
                    DatabaseHelper.instance.delete(getNote.noteId!.toInt());
                    setState(() {
                      listNotes
                          .removeWhere((item) => item.noteId == getNote.noteId);
                    });
                    Navigator.pop(context);
                  },
                  child: Text(Languages.of(context)!.yes)),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)))),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(Languages.of(context)!.cancel)),
            ],
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
          );
        });
  }

  void addNewNote() {
    Route route = MaterialPageRoute(
        builder: (context) => AddEditNote(selectedNote: null, isEdit: false));
    Navigator.push(context, route);
  }
}
