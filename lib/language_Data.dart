class LanguageData {
  late final String flag;
  late final String languageCode;

  LanguageData(this.flag, this.languageCode);

  static List<LanguageData> languageList() {
    return <LanguageData>[
      LanguageData("English", 'en'),
      LanguageData("فارسی", 'fa'),
    ];
  }
}
