# notebook

- Bilingual, Persian and English note application with simple user to record user notes

# This app has the following features :

- Add notes
- show list of notes
- edit notes
- delete notes


- Due to the lack of access to the Mac system, only the settings of the Android platform are applied to the project.

***************************

- اپلیکیشن یادداشت دو زبانه ، فارسی و انگلیسی با کاربری ساده برای ثبت یادداشت های کاربر

- # این برنامه شامل ویژگی های زیر است :

-  افزودن یادداشت
نمایش لیست یادداشت ها -
- ویرایش یادداشت ها
حذف یادداشت ها -


بدلیل عدم دسترسی به سیستم مک ،تنها تنظیمات پلتفرم اندروید بر روی پروژه اعمال شده است. -